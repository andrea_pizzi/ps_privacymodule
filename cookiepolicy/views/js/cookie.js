$(document).ready(function(){

    if (document.cookie.indexOf('cookie_acceptance') > -1 ) {
        console.log("cookie exists");
    }else{
        console.log("cookie doesn't exist");
        display_banner();

        $(".cookies_acceptance").click(function(event) {
            event.preventDefault();
            hide_banner();
        });
    
        $("body a").click(function(event) {
            hide_banner();
        });
    }

    

    function creteCookie(){
        document.cookie = "cookie_acceptance=true";
    }

    function display_banner(){
        $('.privacypolicy_bar').css('display','block');
        $( ".privacypolicy_bar" ).fadeTo( "slow" , 1, function() {
            // Animation complete.
          });

        $( window ).scroll(function() {
            console.log("scrolllll");
            hide_banner();
        });
    }
    function hide_banner(){
        $( ".privacypolicy_bar" ).fadeTo( "fast" , 0, function() {
                $(this).css('display','none');
          });
        creteCookie();
        location.reload();
    }
});