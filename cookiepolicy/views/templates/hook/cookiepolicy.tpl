<div class="privacypolicy_bar {$my_cookie_position}">
    {if isset($my_cookie_title) && $my_cookie_title}
    <h2 class="cookie_title">{$my_cookie_title}</h2>
    {else} {/if}
    <p>
        {$my_cookie_text}
    </p>
    <div class="button_wrp">
        <a href="{$my_cookie_btnurl}"> {if isset($my_cookie_btntxt) && $my_cookie_btntxt} {$my_cookie_btntxt} {else} More Info {/if} </a>
        <a class="cookies_acceptance" href="#">{if isset($my_cookie_btnok) && $my_cookie_btnok} {$my_cookie_btnok} {else} Acceptance {/if}</a>
    </div>
</div>