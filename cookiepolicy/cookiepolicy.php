<?php
if (!defined('_PS_VERSION_'))
{
  exit;
}

class cookiepolicy extends Module
{

   /* @var boolean error */
   protected $_errors = false;
   
  public function __construct()
  {
    
      $this->name = 'cookiepolicy';
      $this->author = 'Pizzigalli Andrea - bePrime.it';
      $this->version = '1.0.0';

      $this->bootstrap = true;
      parent::__construct();

      $this->displayName = $this->getTranslator()->trans('Cookie Policy Banner', array(), 'Modules.Cookiepolicy.Admin');
      $this->description = $this->getTranslator()->trans('Manage cookies policy low banner.', array(), 'Modules.Cookiepolicy.Admin');
      $this->ps_versions_compliancy = array('min' => '1.7.0.0', 'max' => _PS_VERSION_);
  }


  public function install()
  {
    if (Shop::isFeatureActive())
      Shop::setContext(Shop::CONTEXT_ALL);
  
      return parent::install() &&
      $this->registerHook('header');

  }

  public function uninstall()
  {
    if (!parent::uninstall())
    return false;
    return true;
  }



  public function hookHeader()
  {

    $this->context->controller->addCSS($this->_path.'views/css/cookie.css');
    $this->context->controller->addJS($this->_path.'views/js/cookie.js');

    $this->context->smarty->assign(
      array(
          'my_cookie_title' => Configuration::get('COOKIEPOLICY_NAME'),
          'my_cookie_text' => Configuration::get('COOKIEPOLICY_TEXT'),
          'my_cookie_btntxt' => Configuration::get('COOKIEPOLICY_BTNTXT'),
          'my_cookie_btnurl' => Configuration::get('COOKIEPOLICY_BTNURL'),
          'my_cookie_btnok' => Configuration::get('COOKIEPOLICY_BTNOK'),
          'my_cookie_position' => Configuration::get('COOKIEPOLICY_POSITION'),
          
      )
  );

    return $this->display(__FILE__, 'cookiepolicy.tpl');
  }

  public function getContent()
  {
      $output = null;
  
      if (Tools::isSubmit('submit'.$this->name))
      {
          $cookiepolicy = strval(Tools::getValue('COOKIEPOLICY_NAME'));
          $cookiepolicy_text = strval(Tools::getValue('COOKIEPOLICY_TEXT'));
          $cookiepolicy_btntxt = strval(Tools::getValue('COOKIEPOLICY_BTNTXT'));
          $cookiepolicy_btnurl = strval(Tools::getValue('COOKIEPOLICY_BTNURL'));
          $cookiepolicy_btnok = strval(Tools::getValue('COOKIEPOLICY_BTNOK'));
          $cookiepolicy_position = strval(Tools::getValue('COOKIEPOLICY_POSITION'));
          

          if (!$cookiepolicy_text
            || empty($cookiepolicy_text)
            || !Validate::isGenericName($cookiepolicy_text))
              $output .= $this->displayError($this->l('Invalid Configuration value'));
          else
          {
              Configuration::updateValue('COOKIEPOLICY_NAME', $cookiepolicy);
              Configuration::updateValue('COOKIEPOLICY_TEXT', $cookiepolicy_text);
              Configuration::updateValue('COOKIEPOLICY_BTNTXT', $cookiepolicy_btntxt);
              Configuration::updateValue('COOKIEPOLICY_BTNURL', $cookiepolicy_btnurl);
              Configuration::updateValue('COOKIEPOLICY_BTNOK', $cookiepolicy_btnok);
              Configuration::updateValue('COOKIEPOLICY_POSITION', $cookiepolicy_position);
              $output .= $this->displayConfirmation($this->l('Settings updated'));
          }
      }
      return $output.$this->displayForm();
  }


  public function displayForm()
  {
      // Get default language
      $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
  
      // Init Fields form array
      $fields_form[0]['form'] = array(
          'legend' => array(
              'title' => $this->l('Settings'),
          ),
          'input' => array(
              array(
                  'type' => 'text',
                  'label' => $this->l('Cookie Title (optional)'),
                  'name' => 'COOKIEPOLICY_NAME',
                  'size' => 20,
                  'required' => false
              ),
              array(
                'type' => 'textarea',
                'label' => $this->l('Cookie text'),
                'name' => 'COOKIEPOLICY_TEXT',
                'size' => 20,
                'required' => true
              ),
              array(
                'type' => 'text',
                'label' => $this->l('More Info Button text'),
                'name' => 'COOKIEPOLICY_BTNTXT',
                'size' => 20,
                'required' => false
              ),
              array(
                'type' => 'text',
                'label' => $this->l('More Info Button url'),
                'name' => 'COOKIEPOLICY_BTNURL',
                'size' => 20,
                'required' => true
              ),
              array(
                'type' => 'text',
                'label' => $this->l('Acceptance Button'),
                'name' => 'COOKIEPOLICY_BTNOK',
                'size' => 20,
                'required' => false
              ),
              array(
                'type' => 'select',
                'label' => $this->l('Select module position'),
                'name' => 'COOKIEPOLICY_POSITION',
                'required' => true,
                'options' => array(
                    'query' => array(
                        array('key' => 'top_full', 'name' => 'Top Full Width'),
                        array('key' => 'top_left', 'name' => 'Top Left'),
                        array('key' => 'top_right', 'name' => 'Top Right'),
                        array('key' => 'bottom_full', 'name' => 'Bottom Full Width'),
                        array('key' => 'bottom_left', 'name' => 'Bottom Left'),
                        array('key' => 'bottom_right', 'name' => 'Bottom Right'),
                    ),
                    'id' => 'key',
                    'name' => 'name'
                )
              )
          ),
          'submit' => array(
              'title' => $this->l('Save'),
              'class' => 'btn btn-default pull-right'
          )
      );
  
      $helper = new HelperForm();
  
      // Module, token and currentIndex
      $helper->module = $this;
      $helper->name_controller = $this->name;
      $helper->token = Tools::getAdminTokenLite('AdminModules');
      $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
  
      // Language
      $helper->default_form_language = $default_lang;
      $helper->allow_employee_form_lang = $default_lang;
  
      // Title and toolbar
      $helper->title = $this->displayName;
      $helper->show_toolbar = true;        // false -> remove toolbar
      $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
      $helper->submit_action = 'submit'.$this->name;
      $helper->toolbar_btn = array(
          'save' =>
          array(
              'desc' => $this->l('Save'),
              'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
              '&token='.Tools::getAdminTokenLite('AdminModules'),
          ),
          'back' => array(
              'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
              'desc' => $this->l('Back to list')
          )
      );
  
      // Load current value
      $helper->fields_value['COOKIEPOLICY_NAME'] = Configuration::get('COOKIEPOLICY_NAME');
      $helper->fields_value['COOKIEPOLICY_TEXT'] = Configuration::get('COOKIEPOLICY_TEXT');
      $helper->fields_value['COOKIEPOLICY_BTNTXT'] = Configuration::get('COOKIEPOLICY_BTNTXT');
      $helper->fields_value['COOKIEPOLICY_BTNURL'] = Configuration::get('COOKIEPOLICY_BTNURL');
      $helper->fields_value['COOKIEPOLICY_BTNOK'] = Configuration::get('COOKIEPOLICY_BTNOK');
      $helper->fields_value['COOKIEPOLICY_POSITION'] = Configuration::get('COOKIEPOLICY_POSITION');
      
  
      return $helper->generateForm($fields_form);
  }

  
}