# README #

* Prestashop 1.7 Privacy Policy Banner

### What is this repository for? ###

* A privacy policy banner developed for prestashoop 1.7
* vers 1.0.0
* dev by Pizzigalli Andrea (https://www.choosepizzi.com/blog/)

### How do I get set up? ###

* Install and active it.
* Once installed, you can access to a configuration page.
* - Cookie Title ( not required - if is set, you see it, insted you don't see nothing )
* - Cookie text (required ) 
* - More Info Button text ( not required - if is set, you see it, insted you see a default " More Info " text  )
* - More Info Button url (required - it required complate url es: http://www.yoursite.com/ )
* - Acceptance Button text ( not required - if is set, you see it, insted you see a default " Acceptance " text )
* - Module position ( required - Top Fullwidth, Top Left, Top Right, Bottom Fullwidth, Bottom right, Bottom Left  )

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Andrea Pizzigalli
* http://www.choosepizzi.com/ - http://www.choosepizzi.com/blog/